﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nasledovanie
{
    class Program
    {
        static void Main(string[] args)
        {
            Flash flash = new Flash
            {
                Name = "New Flash",
                Model = "New Flash Model",
                Memory = 16384, //16gb
                comingFiles = 1230,
                stor = "Flash ",
            };

            DVD dvd4 = new DVD
            {
                Name = "New DVD",
                Model = "New DVD Model",
                Type = true,
                Memory = 4812, //4,7gb
                comingFiles = 1230,
                stor = "DVD ",
            };

            DVD dvd9 = new DVD
            {
                Name = "New DVD",
                Model = "New DVD Model",
                Type = false,
                Memory = 9216, //9gb
                comingFiles = 1230,
            };

            HDD hdd = new HDD
            {
                Name = "New HDD",
                Model = "New HDD Model",
                Memory = 1048576, //16tb
                comingFiles = 1230,

            };
            Console.WriteLine(flash.FreeMemoryInfo);
            Console.WriteLine(dvd4.FreeMemoryInfo);

            Console.WriteLine(dvd4.InfoCopy);
            Console.WriteLine(dvd9.InfoCopy);
            Console.WriteLine(hdd.InfoCopy);
            Console.WriteLine("Common Memory: " + (flash.Memory+dvd4.Memory+dvd9.Memory+hdd.Memory));
            Console.ReadLine();
        }
    }
}
